**Batch Geocoder**
-----------------
This tool can convert mass addresses to geocodes.

Use Google Map API.

[Site](https://ccjeng.com/geocoder/) 

**License**
[MIT](http://ccjeng.mit-license.org/)
