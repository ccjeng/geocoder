var i;

var split;

var timeoutMS = 900;

var totalRow = 0;
var currRow = 0;

$('.progress').hide();

function trans() {
    i = 0;
    $('#target').val('');
    var a = $('#source').val();
    split = a.split('\n');
    $('.progress').show();
    $('.progress-bar').css('width', 0 +'%').attr('aria-valuenow', 0);   
    totalRow = split.length;

    delayedLoop();
}

function delayedLoop() {
    addressToLatLng(split[i], i);
    if (++i == split.length) return;
    window.setTimeout(delayedLoop, timeoutMS);
}

function addressToLatLng(a, i) {
    var b = new google.maps.Geocoder();
    b.geocode({
        address: a
    }, function(b, c) {
 
        a = '"' + a + '", ';

        if (c == google.maps.GeocoderStatus.OK) {
            var d = $('#target').val();
            $('#target').val(d + a + b[0].geometry.location.lat() + ', ' + b[0].geometry.location.lng() + '\n');
        } else {
            var d = $('#target').val();
            $('#target').val(d + a + 'NA, NA' + '\n');
        }
    });

    //change progress bar
    currRow = i + 1;
    $('.progress-bar').css('width', 100 * currRow / totalRow + '%').attr('aria-valuenow', currRow);   
    $('.progress-bar').text(currRow +' of ' + totalRow);
}

//Facebook Like Button
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=914645301919009";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
